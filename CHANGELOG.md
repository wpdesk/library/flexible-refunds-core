## [1.2.0] - 2025-03-05
### Added
- Upload input files limit
- Hour support to hide the return form
- Email recipients field for admin email template

## [1.1.6] - 2025-02-13
### Fixed
- Problem with calculating the price of taxable products
- Problem with the return of taxable shipments

## [1.1.5] - 2024-11-05
### Fixed
- Placeholder for select field
- PHP Unit lib updated

## [1.1.4] - 2024-09-10
### Fixed
- Auto create stock qty

## [1.1.3] - 2024-05-27
### Fixed
- Removed Public Refund Shortcode from free version

## [1.1.2] - 2024-05-07
### Fixed
- Fixed problem with nonce in shortcode form

## [1.1.1] - 2024-04-07
### Fixed
- Added translations,
- Removed shortcode form if order is found

## [1.1.0] - 2024-03-20
### Added
- Shortcode for refunds without account
### Fixed
- White screen with -1 while requesting refund

## [1.0.28] - 2023-12-27
### Fixed
- submit button
- translations

## [1.0.27] - 2023-12-21
### Fixed
- coupon code

## [1.0.26] - 2023-11-16
### Fixed
- Allow html br in settings descriptions.

## [1.0.25] - 2023-09-12
### Fixed
- added missing class import

## [1.0.24] - 2023-09-12
### Fixed
- correct outputting strings on settings page

## [1.0.23] - 2023-09-12
### Fixed
- added missing class import

## [1.0.22] - 2023-09-12
### Changed
- update links

## [1.0.21] - 2023-09-12
### Changed
- don't show misleading quantity to refund total row in refund request table

## [1.0.20] - 2023-09-12
### Fixed
- improved WooCommerce HPOS integration

## [1.0.19] - 2023-03-29
### Fixed
- trigger email method

## [1.0.18] - 2023-01-30
### Fixed
- refund calculation

## [1.0.17] - 2023-01-30
### Fixed
- my account table view
- refund calculation
## [1.0.16] - 2023-01-19
### Fixed
- critical error

## [1.0.15] - 2023-01-19
### Fixed
- refund table format

## [1.0.14] - 2023-01-11
### Fixed
- refund table calculation
## [1.0.13] - 2022-12-30
### Fixed
- refund table
## [1.0.12] - 2022-12-29
### Fixed
- refund table on my account page

## [1.0.11] - 2022-12-19
### Fixed
- refund table template
## [1.0.10] - 2022-12-15
### Fixed
- refund table on order
## [1.0.9] - 2022-12-12
### Fixed
- refund table mobile responsiveness

## [1.0.8] - 2022-11-21
### Fixed
- backwards compatibility
## [1.0.7] - 2022-11-21
### Added
- support for WooCommerce high performace order storage
## [1.0.6] - 2022-10-27
### Fixed
- critical error
## [1.0.5] - 2022-10-26
### Fixed
- refund forms
## [1.0.4] - 2022-10-06
### Fixed
- email
- refund totals
- discount amount

## [1.0.3] - 2022-09-08
### Fixed
- wc_price
- template overwriting

## [1.0.2] - 2022-08-28
### Fixed
- refund price

## [1.0.1] - 2022-08-04
### Fixed
- escaping data
- calculate shipping tax

## [1.0.0] - 2022-07-27
### Added
- init
