<?php

namespace WPDesk\Library\FlexibleRefundsCore\Helpers;

use WPDesk\Library\FlexibleRefundsCore\Integration;

/**
 * Form builder helper functions.
 */
class FormBuilder {

	/**
	 * @param array $field
	 *
	 * @return array
	 */
	public static function parse_field_args( array $field ): array {
		return wp_parse_args(
			$field,
			[
				'type'        => 'text',
				'name'        => '',
				'label'       => '',
				'html'        => '',
				'enable'      => 1,
				'required'    => 0,
				'options'     => [],
				'placeholder' => '',
				'css'         => '',
				'description' => '',
				'maxlength'   => '',
				'minlength'   => '',
				'files_limit' => 1,
			]
		);
	}

	public static function buttons_field(): array {
		return [
			'text'        => [
				'label' => esc_html__( 'Text', 'flexible-refunds-core' ),
				'free'  => true,
			],
			'textarea'    => [
				'label' => esc_html__( 'Textarea', 'flexible-refunds-core' ),
				'free'  => true,
			],
			'checkbox'    => [
				'label' => esc_html__( 'Checkbox', 'flexible-refunds-core' ),
				'free'  => true,
			],
			'radio'       => [
				'label' => esc_html__( 'Radio', 'flexible-refunds-core' ),
				'free'  => true,
			],
			'select'      => [
				'label' => esc_html__( 'Select', 'flexible-refunds-core' ),
				'free'  => true,
			],
			'multiselect' => [
				'label' => esc_html__( 'Multi Select', 'flexible-refunds-core' ),
				'free'  => Integration::is_super(),
			],
			'upload'      => [
				'label' => esc_html__( 'Upload', 'flexible-refunds-core' ),
				'free'  => Integration::is_super(),
			],
			'html'        => [
				'label' => esc_html__( 'HTML', 'flexible-refunds-core' ),
				'free'  => Integration::is_super(),
			],

		];
	}
}
