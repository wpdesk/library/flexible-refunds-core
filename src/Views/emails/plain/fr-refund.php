<?php
//phpcs:disable
use WPDesk\Library\FlexibleRefundsCore\Helpers\EmailHelper;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly ?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
<?php
echo wpautop( wp_kses( $additional_content, EmailHelper::allowed_tags() ) );
?>
<?php
do_action( 'woocommerce_email_footer' );
