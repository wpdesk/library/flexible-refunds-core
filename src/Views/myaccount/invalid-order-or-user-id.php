<?php defined( 'ABSPATH' ) || exit; ?>
	<h3><?php esc_html_e( 'Invalid request', 'flexible-refunds-core' ); ?></h3>
	<p><?php esc_html_e( 'The order could not be associated with your account.', 'flexible-refunds-core' ); ?></p>
<?php
