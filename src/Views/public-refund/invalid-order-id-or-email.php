<?php defined( 'ABSPATH' ) || exit; ?>

<h3><?php esc_html_e( 'Order not found', 'flexible-refunds-core' ); ?></h3>
<p><?php esc_html_e( 'No order found with associated number and email address', 'flexible-refunds-core' ); ?></p>
