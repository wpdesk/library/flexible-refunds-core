<?php

namespace WPDesk\Library\FlexibleRefundsCore\FormRenderer;

use WPDesk\Forms\Field\InputTextField;

/**
 * HTML field.
 *
 * @package WPDesk\Library\FlexibleRefundsCore\FormRenderer
 */
class HTMLField extends InputTextField {

	public function get_type(): string {
		return 'html';
	}
	/**
	 * @return string
	 */
	public function get_template_name(): string {
		return 'html-input';
	}
}
