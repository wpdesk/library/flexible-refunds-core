<?php

namespace WPDesk\Library\FlexibleRefundsCore\Settings\Tabs;

use WPDesk\Library\FlexibleRefundsCore\Integration;
use WPDesk\View\Renderer\Renderer;
use WPDesk\Library\Marketing\Boxes\MarketingBoxes;

/**
 * Form builder tab.
 */
final class SupportTab extends AbstractSettingsTab {

	const PLUGIN_PRO_SLUG  = 'flexible-refunds-pro';
	const PLUGIN_FREE_SLUG = 'flexible-refund-and-return-order-for-woocommerce';

	const SETTING_PREFIX = 'fr_refund_';

	public function __construct( Renderer $renderer ) {
		parent::__construct( $renderer );
		add_action( 'woocommerce_admin_field_fr_support_settings', [ $this, 'fr_support_settings' ] );
	}

	/**
	 * @return string[][]
	 */
	public function get_fields(): array {
		return [
			[
				'type' => 'fr_support_settings',
			],
		];
	}

	/**
	 * @param $attr
	 *
	 * @return void
	 */
	public function fr_support_settings( $attr ) {
		$local = get_locale();
		if ( $local === 'en_US' ) {
			$local = 'en';
		}
		$slug  = Integration::is_super() ? self::PLUGIN_PRO_SLUG : self::PLUGIN_FREE_SLUG;
		$boxes = new MarketingBoxes( $slug, $local );
		$this->renderer->output_render( 'marketing-page', [ 'boxes' => $boxes ] );
	}

	/**
	 * @return string
	 */
	public static function get_tab_slug(): string {
		return 'support';
	}

	/**
	 * @return string
	 */
	public static function get_tab_name(): string {
		return esc_html__( 'Start Here', 'flexible-refunds-core' );
	}
}
